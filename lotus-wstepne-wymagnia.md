# Wstępne wymagania i propozycje dla organizacji Lotusa

## Co NanoKarrin może zaoferować

* Panele, prelekcje, warsztaty
    * Do podania szczegółów zobowiązujemy się w ciągu dwóch tygodni od uzyskania szczegółów warunków.
    * Co do ilości prawdopodobnie szału nie będzie ze względu na małą liczbę zainteresowanych.
* Koncert
    * Wszystko zależy od warunków jakie dostaniemy, o tym niżej.
    * Prawdopodobnie 2-3 wokalistów i jakaś godzina, może półtorej koncertu.
    * Jeśli koncert ma dojść do skutku trzeba będzie jeszcze ustalić parę rzeczy, ale narazie nie ma sensu na to tracić czasu, skoro jeszcze nie wiadomo w ogóle czy cokolwiek z tego wyjdzie.
* Stoiska nie możemy zrobić ze względu na małą liczbę zainteresowanych ludzi.

## Warunki dla NanoKarrin

* Wejściówki dla ludzi, którzy będą coś robić, prowadzić panele, śpiewać na koncercie etc.
    * Nie spodziewam się żeby tych ludzi było więcej niż piątka.
* Z punktu widzenia wielu ludzi Rzeszów jest daleko i po prostu ciężko im tam dojechać. Zwłaszcza, że większość jechałaby na konwent specjalnie żeby zrobić coś tam związanego z NanoKarrin. Dlatego kluczowa jest kwestia ile i na jakich warunkach Lotus może zwrócić za ten dojazd. Ze swojej strony możemy zagwarantować że będziemy starali się minimalizować te koszty, zarówno jeśli chodzi o liczbę osób jak i o faktyczne koszty na osobę. Od tego co tutaj uzyskamy zależne jest ile faktycznie osób uda się przekonać, a co za tym idzie jaka będzie jakość całego przedsięwzięcia.
