# Harmonogram atrakcji na Lotus

* Koncert przed cosplayem czyli o 17, w związku z tym o 16:30 dobrze by było zrobić przerwę techniczną żebyśmy mogli się rozstawić. Jeśli da się ją wydłużyć do godziny tak żeby zaczynała się o 16 to tym lepiej.
Jeśli nie uda się umówić próby na mainie na wcześniejszą godzinę to trzeba ją będzie zrobić bezpośrednio przed koncertem co wydłuży soundcheck o jakąś godzinę.
* After Party po koncercie i najlepiej po cosplayu żeby nie robić konkurencji czyli pewnie koło 21. Da to też czas żeby przenieść sprzęt i chwilę wytchnienia dla wokalistów.
* Rozgrzewka głosu nie mniej niż dwie godziny przed koncertem czyli najlepiej przed 14.

Poza tym nie mamy szczególnych preferencji co do godzin atrakcji. Żadne atrakcje NanoKarrin nie powinny na siebie zachodzić. 