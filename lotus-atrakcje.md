# Propozycja atrakcji na Lotus

## Koncert NanoKarrin
### Opis do informatora
Specjalnie dla was, specjalnie w tę noc – koncert piosenek z fanowskimi tłumaczeniami! Zanurzcie się z nami w (paradoksalny to) świat japońskiej muzyki po polsku i – jeśli się odważycie – śpiewajcie razem z nami!
### Czas trwania
Sam koncert będzie trwał jakąś godzinę, ale trzeba zarezerwować trochę czasu na soundcheck i ostatnie ustawienia, więc warto policzyć nawet 2 godziny.
### Twórcy atrakcji
* Laura "Amianka" Posemkiewicz
* Anita "yuyechka" Szyszka
* Szymon "cheeseman" Korzeniowski
* Michał "Orzecho" Dąbrowski (obsługa techniczna)

### Wymagania i uwagi
Przed koncertem trzeba zrobić soundcheck, idealnie by było przy zamkniętych drzwiach (15-30 minut). Wcześniej trzeba też zrobić godzinną próbę, najlepiej chyba będzie rano żeby nie psuła za bardzo programu. Potrzebujemy dokładnego opisu nagłośnienia na mainie żeby wiedzieć na co musimy się przygotować.

## Konkurs dubbingowy
### Opis do informatora
Masz głos z gumy, wymyślasz scenariusze na poczekaniu i generalnie uważasz, że zrobiłbyś to wszystko lepiej? Przyjdź i stań w szranki w konkursie fandubbingowym!
### Czas trwania
2 godziny
### Twórcy atrakcji
* Michał "Orzecho" Dąbrowski
* Jola "Pchełka" Dereń

### Wymagania i uwagi
Komputer, projektor, głośniki.

## Warsztaty tłumaczenia piosenek
### Opis do informatora
Chciałeś kiedyś napisać polski tekst do ulubionej piosenki? Przyjdź na warsztaty a dowiesz się jak to zrobić!
### Czas trwania
1 godzina
### Twórcy atrakcji
* Jola "Pchełka" Dereń

### Wymagania i uwagi  
Komputer, projektor, głośniki.

## Polski fandubbing od czasów Mieszka
### Opis do informatora
Fandubbing w Polsce nie urodził się wczoraj. Ludzie czuli potrzebę tłumaczenia i nagrywania swojego głosu od kiedy było to możliwe. Na tym panelu możesz poznać ich perypetie, przynajmniej te z ostatnich 10 lat.
### Czas trwania
1 godzina
### Twórcy atrakcji
* Michał "Orzecho" Dąbrowski

### Wymagania i uwagi
Komputer, projektor, głośniki.

## Co robimy lepiej od Japonii
### Opis do informatora
Jako fani kultury japońskiej często idealizujemy obiekt naszego zainteresowania i skupiamy się na jego atutach, pomijając wady. Bliskie spotkanie trzeciego stopnia uświadamia nas jednak, że nie wszystko jest takie, jakie byśmy oczekiwali.
### Czas trwania
1 godzina
### Twórcy atrakcji
* Michał "Orzecho" Dąbrowski

### Wymagania i uwagi
Komputer, projektor, głośniki. Ten panel nie ma żadnego związku z NanoKarrin i fandubbingiem jako takim.

## Proces tworzenia fanddubingu
### Opis do informatora
Dowiesz sie jak wyglądają kolejne etapy tworzenia amatorskiego dubbingu, jak się do tego zabrać i jakich narzędzi użyć.
### Czas trwania
2 godziny
### Twórcy atrakcji
* Dariusz "konji" Garbarz

### Wymagania i uwagi
Komputer, projektor, głośniki. 

## After Party
### Opis do informatora
Jeśli po koncercie nie masz nas dość, wpadnij na ciąg dalszy! Posłuchaj utworów
w aranżacji akustycznej lub zaśpiewaj z nami. Zintegruj się z grupą i przekaż nam, co możemy zrobić,
by działać jeszcze lepiej!
### Czas trwania
3 godziny
### Twórcy atrakcji
* Michał "Orzecho" Dąbrowski

### Wymagania i uwagi

## Rozgrzewka głosu
### Opis do informatora
Chcesz poznać ćwiczenia z emisji głosu i dowiedzieć się jak je poprawnie wykonywać? Nie musisz umieć śpiewać, aby dołączyć. Pierwsza połowę zajęć poświęcimy na emisję głosu od strony aktorskiej i wystpień publicznych, natomiast druga część będzie poświęcona ćwiczeniom wokalnym.
### Czas trwania
1,5 godziny
### Twórcy atrakcji
* Laura "Amianka" Posemkiewicz

### Wymagania i uwagi

## Wolne dubbingowanie
### Opis do informatora
Chcesz spróbować swoich sił za mikrofonem i usłyszeć swój głos w ulubionej piosence lub scence? Wpadnij do nas, da się załatwić!
### Czas trwania
4 godziny
### Twórcy atrakcji
* Michał "Orzecho" Dąbrowski
* Jola "Pchełka" Dereń

### Wymagania i uwagi
Potrzebujemy przed atrakcją pół godziny na zainstalowanie sprzętu.